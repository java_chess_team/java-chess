package ai;

import board.BoardPosition;

public class Move {
	private BoardPosition from;
	private BoardPosition to;
	
	public Move(BoardPosition from, BoardPosition to) {
		this.from = from;
		this.to = to;
	}

	public BoardPosition getFrom() {
		return from;
	}

	public BoardPosition getTo() {
		return to;
	}
	
	public String toString() {		
		return this.from.toString() + " -> " + this.to.toString();
	}

}
