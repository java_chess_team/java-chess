package board;

/**
 * a generic board with a custom constructor
 * just for not having to call {@link board.BoardSchema#buildBoardStandard()} 
 * every time
 *
 */
public class StandardBoard extends GenericBoard{

	public StandardBoard() throws Exception {
		super(BoardSchema.buildBoardStandard());
	}
	
} 