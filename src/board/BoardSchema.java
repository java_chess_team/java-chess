package board;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import piece.Bishop;
import piece.Cell;
import piece.CellImpl;
import piece.King;
import piece.Knight;
import piece.Pawn;
import piece.Queen;
import piece.Rook;
import player.PlayerColor;

/**
 * this class is used to instantiate Array of Array (matrix) of Cell(s)
 * it uses a custom string format
 * 
 * format description below in the first method
 *
 */
public class BoardSchema {
	
	/**
	 * LEGEND:
	 * 
	 * character has to be grouped in pairs except for the end of a line
	 * which is just a '/'
	 * 
	 * every other pair is divided in two kind
	 * 
	 * first kind:
	 * piece are a letter/number 
	 * the letter is a representation of the piece
	 * the number is an identification for the player
	 *  
	 * examples: 
	 * P1 = first's player pawn (in owr case, always the white player) 
	 * Q2 = second's player queen (always the black)
	 * N3 = a knight for a third player
	 * 
	 * second kind:
	 * a double '-' is an empty position
	 * a double '.' is a position where no piece can be positioned
	 * 
	 * IMPORTANT:
	 * every string must represent a rectangular board
	 * for simplicity reasons
	 */
	private static String stdBoard = ""
			+ "R0N0B0Q0K0B0N0R0/"
			+ "P0P0P0P0P0P0P0P0/"
			+ "----------------/"
			+ "----------------/"
			+ "----------------/"
			+ "----------------/"
			+ "P1P1P1P1P1P1P1P1/"
			+ "R1N1B1Q1K1B1N1R1";

	
	/**
	 * builds a standard board with all
	 * pieces already in their right position
	 */
	public static ArrayList<ArrayList<Cell>> buildBoardStandard() throws Exception {
		return BoardSchema.buildBoard(BoardSchema.stdBoard);
	}

	/**
	 * @param boardSchema - a representation of a board in the format described above
	 * @return the matrix of {@link piece.Cell} the form the board
	 * @throws Exception raised if it's found a character that cannot be converted in a cell
	 * or if the board does not result rectangular
	 */
	public static ArrayList<ArrayList<Cell>> buildBoard(String boardSchema) throws Exception {
		//qui gli metto tutte le righe
		ArrayList<ArrayList<Cell>> wannaBeBoard = new ArrayList<>();
		//divido tutta la stringa in righe della scacchiera
		Iterator<String> rowIterator = Arrays.stream(boardSchema.split("/")).iterator();
	    int i = 0;
	    int j = 0;
	    
		while(rowIterator.hasNext()) {//scorro riga per riga
			String row = rowIterator.next();
			
			ArrayList<Cell> wannaBeRow = new ArrayList<>();//creo un ph per la riga
			Iterator<String> columnIterator = Arrays.stream(row.split("")).iterator();
			
			while(columnIterator.hasNext()) {
				//prendu due caratteri: uno per il pezzo, uno per il giocatore
				String pcsString = columnIterator.next() + columnIterator.next();
				BoardPosition bp = new BoardPosition(i, j);

				//ora ho un pezzo e le sue coordinate
				//devo creare la cella
							
				
				switch (pcsString) {
					case "..":{ // cella vuota non riempibile
						wannaBeRow.add(new CellImpl(false,  bp));
						break;
					}
					case "--":{ // cella vuota riempibile
						wannaBeRow.add(new CellImpl(true,  bp));
						break;
					}
					default:{//ovvero se c'è una lettera
						wannaBeRow.add(buildCellWithPiece(pcsString,bp));
						break;
					}
				}
				j++;
			}
			j=0;
			wannaBeBoard.add(wannaBeRow);
			i++;
		}

		BoardSchema.checkDimensions(wannaBeBoard);
		
		return wannaBeBoard;
	}
	
	private static void checkDimensions(ArrayList<ArrayList<Cell>> board) throws Exception{
		//deve essere rettangolare.. il resto non mi frega
		int numCols = board.get(0).size(); // prendo la prima riga, e guardo quanti posti ci sono
		for(List<Cell> r : board) 
			if(!(r.size() == numCols))
				throw new Exception("Scacchiera non quadrata");
		//poi del numero di righe non mi importa
	}
	
	private static Cell buildCellWithPiece(String pcReppr, BoardPosition bp) throws Exception {
		PlayerColor pc = null;
		switch (pcReppr.charAt(1)) {
			case '0': {pc = PlayerColor.WHITE; break;}
			case '1': {pc = PlayerColor.BLACK; break;}
			default : throw new Exception("uops,la stringa che rappresenta la tastiera non � stata fatta benissimo ");
		}
		
		switch (pcReppr.charAt(0)) {
			case 'P': return new CellImpl(true, new Pawn(pc, bp), bp);  
			case 'R': return new CellImpl(true, new Rook(pc, bp), bp);  
			case 'B': return new CellImpl(true, new Bishop(pc, bp), bp);  
			case 'Q': return new CellImpl(true, new Queen(pc, bp), bp);  
			case 'K': return new CellImpl(true, new King(pc, bp), bp);  
			case 'N': return new CellImpl(true, new Knight(pc, bp), bp);  
			default : throw new Exception("uops,la stringa che rappresenta la tastiera non � stata fatta benissimo (ed ha un pezzo che non esiste) ");
		}
	
	}
	 
		
	
	
	
	
}
