package board;


import java.util.Optional;

import piece.Piece;
import player.PlayerColor;

/**
 * left just for compatibility,
 * used as generalizaion for 'pre-built' board
 * then i started using {@link board.BoardSchema}  
 */
@Deprecated
public interface Board {
	
	public Optional<PlayerColor> getPieceColorAtPosition(BoardPosition bp);
		
	public boolean playersKingIsInCheck(PlayerColor kingColor);
	
	public boolean playersKingIsInCheckMate(PlayerColor kingColor); 
	
	public boolean matchIsInStaleMate();
	
	public void movePiece(BoardPosition from, BoardPosition to) throws Exception;
	
	public Optional<Piece> getPieceAt(BoardPosition bp);
	

}
