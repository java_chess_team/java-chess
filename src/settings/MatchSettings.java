package settings;

/**
 * 
 * @author Angela Cortecchia
 *
 */
public class MatchSettings 
{	
	public enum Difficulty {
		EASY,
		NORMAL,
		HARD
	}
	public enum GameMode {
		STANDARD
	}
	public enum GameTime {
		disabled,
		max10Seconds,
		max30Sedonds,
		max2Minutes,
		max5Minutes,
		max10Minutes
	}
	public enum PiecesSkin {
		STANDARD,
		CORVOSTUDIO
	}
}
