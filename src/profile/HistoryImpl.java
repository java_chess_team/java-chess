package profile;
/**
 * 
 * @author Angela Cortecchia
 *
 */
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import profile.PlayerImpl;

public class HistoryImpl implements History, Serializable {

	private static final long serialVersionUID = 1L;
	public List<PlayerImpl> matches = new ArrayList<>();

	/**
	 * 
	 * @param scores
	 * method to add a match in 'history'
	 */
	public void addMatch(PlayerImpl scores) {
		matches.add(scores); 
	}
	
	/**
	 * 
	 * @throws IOException
	 */
	public void serialize() throws IOException {
		FileOutputStream fos = new FileOutputStream("History");
		ObjectOutputStream oos = new ObjectOutputStream(fos);
		oos.writeObject(this);
		oos.close();
		fos.close();
	}
	
	/**
	 * 
	 * @return 
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public static HistoryImpl deserialize() throws ClassNotFoundException, IOException {
		try
		{
			FileInputStream fis = new FileInputStream("History");
			ObjectInputStream ois = new ObjectInputStream(fis);
			HistoryImpl history = (HistoryImpl) ois.readObject();
			fis.close();
			ois.close();
			return history;
		}
		catch(IOException e)
		{
			System.out.println("No file saved found!");
		}
		return new HistoryImpl();
	}

	/**
	 * method to delete all the matches saved in 'history'
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	public void deleteHistory() throws ClassNotFoundException, IOException {
		this.matches.clear();
		this.serialize();
	}
}