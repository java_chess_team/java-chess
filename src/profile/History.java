package profile;
/**
 * 
 * @author Angela Cortecchia
 *
 */
import java.io.IOException;

public interface History {

	/**
	 * 
	 * @param scores
	 * method to add a match in 'history'
	 */
	void addMatch(PlayerImpl scores);
	
	/**
	 * method to delete all the matches saved in 'history'
	 * @throws ClassNotFoundException
	 * @throws IOException
	 */
	void deleteHistory() throws ClassNotFoundException, IOException;
}
