package profile;

/**
 * 
 * @author Angela Cortecchia
 *
 */
import java.util.*;
import settings.MatchSettings.Difficulty;;

public interface Player {
	
	/**
	 * @return the difficulty of the match, chosen by the player
	 */
	Difficulty getDiff();
	
	/**
	 * @return a String with the name of the winner
	 */
	String getWinner();
	
	/**
	 * @return a String with the name of the loser
	 */
	public String getLoser();
	
	/**
	 * @return an integer with the final score of the winner
	 */
	int getWinnerScore();
	
	
	/**
	 * @return an integer with the final score of the loser
	 */
	int getLoserScore();
	
	/**
	 * @return the date of the match when it took place
	 */
	Date getDate();
}
