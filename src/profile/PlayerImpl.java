package profile;

import settings.MatchSettings.Difficulty;

import java.io.Serializable;
import java.util.Date;
import logic.MatchScore;

/**
 * 
 * @author Angela Cortecchia
 * 
 * class 
 *
 */
public class PlayerImpl implements Player, Serializable {

	private static final long serialVersionUID = 1L;
	public MatchScore scores;
	public Difficulty diff;
	
	/**
	 * 
	 * @param scores
	 */
	public PlayerImpl(MatchScore scores) {
		this.scores = scores;
		this.diff = player.UserImpl.difficulty;
	}
	
	/**
	 * @return the difficulty of the match, chosen by the player
	 */
	
	public Difficulty getDiff() {
		return this.diff;
	}

	/**
	 * @return a String with the name of the winner
	 */
	public String getWinner() {
		if(scores.finalScore1() > scores.finalScore2()) {
			return scores.player1();
		}else {
			return scores.player2();
		}
	}
	
	/**
	 * @return a String with the name of the loser
	 */
	public String getLoser() {
		if(scores.finalScore1() > scores.finalScore2()) {
			return scores.player2();
		}else {
			return scores.player1();
		}
	}
	
	/**
	 * @return an integer with the final score of the winner
	 */
	public int getWinnerScore() {
		if(getWinner() == scores.player1()) {
		return this.scores.finalScore1();
		}else {
			return this.scores.finalScore2();
		}
	}
	
	/**
	 * @return an integer with the final score of the loser
	 */
	public int getLoserScore() {
		if(getWinner() != scores.player1()) {
			return scores.finalScore1();
		}else {
			return scores.finalScore2();
		}
	}

	/**
	 * @return the date of the match when it took place
	 */
	public Date getDate() {
		return scores.getDate();
	}
}
