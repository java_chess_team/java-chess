package piece;

import java.util.List;

import board.BoardPosition;
import board.GenericBoard;
import player.PlayerColor;

public interface Piece{

	/**
	 * Returns a owner of the Piece
	 * @return a PlayerColor
	 */
	public PlayerColor getOwner();
	
	/**
	 * Returns a positions of the piece
	 * @return a BoardPosition
	 */
	public BoardPosition getPosition();
	
	/**
	 * Set a position of the piece
	 * @param position is a BoardPosition
	 */
	public void setPosition(final BoardPosition position);
	
	/**
	 * Returns a possible moves of the piece in a board
	 * @param board is a GenericBoard
	 * @return a List<BoardPosition>, a list of BoardPosition
	 */ 
	public abstract List<BoardPosition> getPossibleMoves(GenericBoard board);
	
	/**
	 * Returns a icon of the piece
	 * @return a String
	 */
	public abstract String getIcon();
	
	/**
	 * Returns a value of the piece
	 * @return a int value
	 */
	public abstract int getScore();
	
	public abstract String toString();
}