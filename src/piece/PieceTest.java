package piece;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

import board.BoardPosition;
import player.PlayerColor;

class PieceTest {

	private Piece piece;
	private BoardPosition position;
	
	@Test
	void testPieceBoardPosition( ){
		this.position = new BoardPosition(2, 2);
		this.piece = new Pawn(PlayerColor.BLACK, this.position);
		assertEquals(this.position, this.piece.getPosition());
	}
	
	@Test
	void testPieceOwner() {
		this.position = new BoardPosition(2, 2);
		this.piece = new Pawn(PlayerColor.BLACK, this.position);
		assertEquals(PlayerColor.BLACK, this.piece.getOwner());
	}

}
