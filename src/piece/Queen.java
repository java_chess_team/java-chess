package piece;

import java.util.ArrayList;
import java.util.List;

import board.BoardPosition;
import board.GenericBoard;
import player.PlayerColor;

public class Queen extends AbstractPiece{

	public Queen() {
		super();
	}
	
	public Queen(PlayerColor owner, BoardPosition position) {
		super(owner, position);
	}

	@Override
	public List<BoardPosition> getPossibleMoves(GenericBoard board) {
		List<BoardPosition> possibleMoves = new ArrayList<>();
		
		int y = position.getY();
		y++;
		while(new BoardPosition(position.getX(), y).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(position.getX(), y)).isPresent())
				possibleMoves.add(new BoardPosition(position.getX(), y));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(position.getX(), y)).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(position.getX(), y));
					break;
				}
			}
			y++;	
		}
		
		y = position.getY();
		y--;
		while(new BoardPosition(position.getX(), y).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(position.getX(), y)).isPresent())
				possibleMoves.add(new BoardPosition(position.getX(), y));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(position.getX(), y)).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(position.getX(), y));
					break;
				}
			}
			y--;
		}
		
		int x = position.getX();
		x++;
		while(new BoardPosition(x, position.getY()).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(x, position.getY())).isPresent())
				possibleMoves.add(new BoardPosition(x, position.getY()));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(x, position.getY())).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(x, position.getY()));
					break;
				}
			}
			x++;
		}
		
		x = position.getX();
		x--;
		while(new BoardPosition(x, position.getY()).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(x, position.getY())).isPresent())
				possibleMoves.add(new BoardPosition(x, position.getY()));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(x, position.getY())).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(x, position.getY()));
					break;
				}
			}
			x--;
		}
		
		x = position.getX();
		y = position.getY();
		x++;
		y++;
		while(new BoardPosition(x, y).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(x, y)).isPresent())
				possibleMoves.add(new BoardPosition(x, y));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(x, y)).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(x, y));
					break;
				}
			}
			x++;
			y++;	
		}
		
		x = position.getX();
		y = position.getY();
		x++;
		y--;
		while(new BoardPosition(x, y).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(x, y)).isPresent())
				possibleMoves.add(new BoardPosition(x, y));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(x, y)).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(x, y));
					break;
				}
			}
			x++;
			y--;	
		}
		
		x = position.getX();
		y = position.getY();
		x--;
		y++;
		while(new BoardPosition(x, y).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(x, y)).isPresent())
				possibleMoves.add(new BoardPosition(x, y));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(x, y)).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(x, y));
					break;
				}
			}
			x--;
			y++;	
		}
		
		x = position.getX();
		y = position.getY();
		x--;
		y--;
		while(new BoardPosition(x, y).isValidClassicBoardPositions()) {
			if(!board.getPieceColorAtPosition(new BoardPosition(x, y)).isPresent())
				possibleMoves.add(new BoardPosition(x, y));
			else {
				if(board.getPieceColorAtPosition(new BoardPosition(x, y)).get().equals(owner))
					break;
				else {
					possibleMoves.add(new BoardPosition(x, y));
					break;
				}
			}
			x--;
			y--;	
		}
		
		return possibleMoves;
	}

	@Override
	public String getIcon() {
		return "Queen.png";
	}

	@Override
	public int getScore() {
		return 9;
	}
	
	@Override
	public String toString() {
		return "q";
	}

}
