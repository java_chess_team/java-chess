package gui;
 
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.event.ActionListener;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import settings.Translations;

public class HelpGUI extends JFrame{
	
	private static final long serialVersionUID = 1L;
	private JButton close;
	private JPanel layout;
	private JLabel text;

	public HelpGUI() {
		super("Help");
		GridBagConstraints grid = new GridBagConstraints();
		this.close = new JButton("Close");
		this.text = new JLabel();
		this.layout = new JPanel(new GridBagLayout());
		this.close.addActionListener(ac);
		
        this.layout.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "<html><font color='white'>Tutorial"));
        this.layout.setOpaque(true);
        this.layout.setBackground(Color.GRAY);
        this.layout.setForeground(Color.BLACK);
        

        this.close.setBackground(Color.WHITE);
        this.close.setForeground(Color.BLACK);
		
		grid.gridx = 2;
		grid.gridy = 4;
		this.layout.add(close, grid);
		
		grid.gridx = 1;
		grid.gridy = 1;
		this.layout.add(this.text, grid);
		
		this.text.setText(Translations.getTranslation("help1"));
		
		this.add(layout);
		
        this.setSize(800, 600);
        this.pack();
        this.setVisible(true);
	}
	
	ActionListener ac = (e)-> {
		this.setVisible(false);
	};
}
