package gui;

/**
 * 
 * @author Marco Amadei
 * 
 * GUI per il menu principale
 *
 */
import javax.swing.*;

import Sound.Audio;
import settings.CrossPlatformResources;

import java.awt.Color;
import java.awt.Frame;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.Toolkit;

public class MenuGUI extends JFrame{


	private static final long serialVersionUID = 1L;
	
	//bottoni
    private JButton newGame = new JButton("New Game");
    private JButton settings = new JButton("Settings");
    private JButton statistics = new JButton("Statistics");
    private JButton closeAll = new JButton("Close");
    private JLabel up_title = new JLabel("<html><h1><font color='white'>Java Chess</h1></font>");
    
    //variabili di supporto
	static SettingsGUI openedSettings=null;
    
    
    public MenuGUI(){
        super("Java Chess");//chiama costruttore di base

        for (Frame f: Frame.getFrames())
        	f.setIconImage(Toolkit.getDefaultToolkit().getImage(CrossPlatformResources.getResourceFile("/gui/icon.png").getPath()));
        
        //SETUP MAIN PANEL
        JPanel MainPanel = new JPanel(new GridBagLayout());
        MainPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "<html><font color='white'>Menu"));
        GridBagConstraints constraints = new GridBagConstraints();
        MainPanel.setOpaque(true);
        MainPanel.setBackground(Color.GRAY);
        MainPanel.setForeground(Color.BLACK);

        constraints.anchor = GridBagConstraints.CENTER;
        constraints.insets = new Insets(10, 10, 10, 10);

        //TITLE
        constraints.gridx = 1;
        constraints.gridy = 0;
        MainPanel.add(up_title,constraints);

        //new game button
        newGame.addActionListener(e -> {CreateNewGame();});
        new TextToTranslationJButton(newGame,"newgame");
        constraints.gridx = 1;
        constraints.gridy = 1;
        newGame.setBackground(Color.WHITE);
        newGame.setForeground(Color.BLACK);
        MainPanel.add(newGame,constraints);
        
        //Statistics window button
        statistics.addActionListener(e -> {WatchStatistics();});
        statistics.setBackground(Color.WHITE);
        statistics.setForeground(Color.BLACK);
        new TextToTranslationJButton(statistics,"Statistics");
        constraints.gridx = 1;
        constraints.gridy = 2;
        MainPanel.add(statistics,constraints);
        
        //Settings window button
        settings.addActionListener(e -> {OpenSettingsGUI();});
        settings.setBackground(Color.WHITE);
        settings.setForeground(Color.BLACK);
        new TextToTranslationJButton(settings,"settings");
        constraints.gridx = 1;
        constraints.gridy = 3;
        MainPanel.add(settings,constraints);

        //Settings window button
        closeAll.addActionListener(e -> {closeGame();});
        closeAll.setBackground(Color.WHITE);
        closeAll.setForeground(Color.BLACK);
        new TextToTranslationJButton(closeAll,"close");
        constraints.gridx = 1;
        constraints.gridy = 4;
        MainPanel.add(closeAll,constraints);
       
        
        //FRAME OPTIONS
        add(MainPanel);
        setSize(800, 600);
        pack();
        setVisible(true);

        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                closeGame();
            }
        });
    }
    

    /**
     * Open a settings modifier dialog.
     */
	private void OpenSettingsGUI(){
		Audio.playSound("/clips/button_01.wav");
		
		if (openedSettings!=null){
			openedSettings.OpenSettings();
		}
		else
		{
			openedSettings=new SettingsGUI();
		}
	}

    /**
     * Open a create game dialog.
     */
    private void CreateNewGame(){
		Audio.playSound("/clips/button_01.wav");
    	new NewGameConfigGUI();
    }


    /**
     * open a statistics dialog.
     */
    private void WatchStatistics()
    {
		Audio.playSound("/clips/button_01.wav");
		new StatisticsGUI().setVisible(true);
    }

    /**
     * Close the process
     */
    private void closeGame()
    {
    	System.exit(0);
    }
}
