package gui;

/**
 * 
 * @author Marco Amadei
 * 
 * GUI del menu delle impostazioni
 *
 */
import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.*;

import Sound.Audio;
import settings.*;

public class SettingsGUI extends JFrame
{
    
    //bottoni
	private static final long serialVersionUID = 1L;
    private JButton close = new JButton("Close");
    private JLabel up_title = new JLabel("<html><font color='yellow'>Settings</font><font color='red'></font>");
    private JComboBox<String> languageDropDown= new JComboBox<>();
    JCheckBox hintsEnabled = new JCheckBox("Hints enabled");
    JCheckBox soundsEnabled = new JCheckBox("Sounds enabled");
    
    //variabili di supporto
    
    public SettingsGUI(){
        super("Java Chess");//chiama costruttore di base
   
        
        //SETUP MAIN PANEL
        JPanel MainPanel = new JPanel(new GridBagLayout());
        MainPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createEtchedBorder(), "<html><font color='white'>Settings"));
        GridBagConstraints constraints = new GridBagConstraints();
        MainPanel.setOpaque(true);
        MainPanel.setBackground(Color.GRAY);
        MainPanel.setForeground(Color.BLACK);

        constraints.anchor = GridBagConstraints.CENTER;
        constraints.insets = new Insets(10, 10, 10, 10);

        //TITLE
        constraints.gridx = 0;
        constraints.gridy = 0;
        new TextToTranslationJLabel(up_title,"settings");
        MainPanel.add(up_title,constraints);
        up_title.setForeground(Color.white);

        //close button
        close.addActionListener(e -> {SaveAndClose();});
        new TextToTranslationJButton(close,"close");
        constraints.gridx = 0;
        constraints.gridy = 5;
        close.setBackground(Color.WHITE);
        close.setForeground(Color.BLACK);
        MainPanel.add(close,constraints);


        //SETUP LANGUAGE SCROLL DOWN
        for (settings.Translations.Language lang : Translations.Language.values()) 
        	languageDropDown.addItem(lang.toString());
        ActionListener al = new ActionListener(){
            @Override
            public void actionPerformed(ActionEvent e) {
                @SuppressWarnings("rawtypes")
                String item = ((String)((JComboBox)e.getSource()).getSelectedItem());
                OnLanguageChanged(item);
            }
        };
        languageDropDown.addActionListener(al);

        languageDropDown.setBackground(Color.WHITE);
        languageDropDown.setForeground(Color.BLACK);
        constraints.gridx = 0;
        constraints.gridy = 2;
        MainPanel.add(languageDropDown,constraints);
        

        //enable hints 
        hintsEnabled.addActionListener(e -> {EnableHints(hintsEnabled.isSelected());});
        new TextTotranslationJCheckBox(hintsEnabled,"hintsenabled");
        constraints.gridx = 0;
        constraints.gridy = 3;
        hintsEnabled.setBackground(Color.WHITE);
        hintsEnabled.setForeground(Color.BLACK);
        MainPanel.add(hintsEnabled,constraints);
        hintsEnabled.setSelected(true);//METTERE CHE INVECE GUARDA A COME SONO MESSE LE SETTINGS

        //enable sounds 
        soundsEnabled.addActionListener(e -> {EnableSounds(soundsEnabled.isSelected());});
        new TextTotranslationJCheckBox(soundsEnabled,"soundsenabled");
        constraints.gridx = 0;
        constraints.gridy = 4;
        soundsEnabled.setBackground(Color.WHITE);
        soundsEnabled.setForeground(Color.BLACK);
        MainPanel.add(soundsEnabled,constraints);
        soundsEnabled.setSelected(true);//METTERE CHE INVECE GUARDA A COME SONO MESSE LE SETTINGS
        
        //FRAME OPTIONS
        add(MainPanel);
        setSize(800, 600);
        pack();
        setVisible(true);
    }

    /**
     * Update all translations on screen to the selected language.
     * @param newLanguage Selected language.
     */
	private void OnLanguageChanged(String newLanguage)	{
		
    	Translations.setlanguage(Translations.Language.valueOf(newLanguage));
    }

    /**
     * Enable on game possible moves visualization.
     * @param _enabled True to enable hints.
     */
	private void EnableHints(boolean _enabled)	{
		
		settings.GlobalSettingsImpl.isHintEnabled = _enabled;
	}

    /**
     * Enable game sounds.
     * @param _enabled true to enable sounds.
     */
	private void EnableSounds(boolean _enabled)	{
		
		settings.GlobalSettingsImpl.isAudioEnabled = _enabled;
	}

    /**
     * Keep the current settings.
     */
    private void SaveAndClose()	{

		Audio.playSound("/clips/button_01.wav");
    	setVisible(false); //you can't see me!
    }

    /**
     * Show this settings panel.
     */
    public void OpenSettings()	{
    	
    	setVisible(true);
    }
}