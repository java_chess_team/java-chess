package gui;

/**
 * 
 * @author Marco Amadei
 * 
 * GUI per la scacchiera
 *
 */
import java.awt.Color;
import java.util.List;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.GridLayout;
import java.awt.Insets;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Optional;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;

import board.BoardPosition;
import logic.MatchManager;
import piece.Cell;
import piece.Piece;
import player.PlayerColor;
import settings.CrossPlatformResources;
import settings.Translations;

public class BoardGUI extends JFrame
{
	MatchManager matchManager;
	 
	//robe
	private static int squareSize=40;
    
    //bottoni
	private static final long serialVersionUID = 1L;
    private JButton[][] celle;
    private JButton help=new JButton("Help");
    private JButton close=new JButton("Surrender");
    private JLabel turno = new JLabel("<html><font color='yellow'>Turno</font><font color='red'><br>Player 1");
    private JLabel time = new JLabel("<html><font color='white'></font>");
    private JLabel eatList = new JLabel("<html><font color='yellow'></font>");
    
    private BoardPosition lastSelectedCell=null;

    int maxI=8;
    int maxJ=8;
    
    /**
     * @param matchManager instance of a MatchManager to connect with.
     */
    public BoardGUI(MatchManager matchManager){
        super("Java Chess");//chiama costruttore di base
        
        this.matchManager=matchManager;

        //SETUP MAIN PANEL
        JPanel MainPanel = new JPanel(new GridBagLayout());
        String gameNum = "Match "+matchManager.getMatchNumberInfo();
        MainPanel.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(), "<html><font color='white'>"+gameNum));
        GridBagConstraints constraints = new GridBagConstraints();
        MainPanel.setOpaque(true);
        MainPanel.setBackground(Color.GRAY);
        MainPanel.setForeground(Color.BLACK);

        constraints.anchor = GridBagConstraints.CENTER;
        constraints.insets = new Insets(10, 10, 10, 10);

       JPanel board= new JPanel(new GridLayout(maxI,maxJ));
       board.setBorder(new LineBorder(Color.BLACK));
       celle=new JButton[maxI][maxJ];
        for (int i=0; i< maxI; i++)
        {
        	for (int j=0; j<maxJ; j++)//crea in base a com� la scacchiera e a quali pezzi ci sono
        	{
        		celle[i][j]=new JButton("");//Creare cella in base a tipo di cella che sta effettivamente nella scacchiera
        		celle[i][j].addActionListener(e -> {onCellPressed(findButton( e.getSource() ));});
        		setCellColor(i,j);
        		celle[i][j].setPreferredSize(new Dimension(squareSize,squareSize));
        		celle[i][j].setSize(squareSize, squareSize);
                constraints.gridx = i;
                constraints.gridy = j;
                celle[i][j].setBorder(null);
                board.add(celle[i][j],constraints);
                setSprite(celle[i][j],null);
                
        	}
        }
        constraints.gridx = 1;
        constraints.gridy = 0;
        MainPanel.add(board,constraints);

        //help button
        help.setBackground(Color.WHITE);
        help.setForeground(Color.BLACK);
        help.addActionListener(e -> {openHelp();});
        constraints.gridx = 0;
        constraints.gridy = maxJ/2;
        MainPanel.add(help,constraints);
        new TextToTranslationJButton(help, "help");
        
        //close button
        close.setBackground(Color.WHITE);
        close.setForeground(Color.BLACK);
        close.addActionListener(e -> {
        	matchManager.getGameLogic().endGame(matchManager.getGameLogic().getCurrentPlayerColor()==PlayerColor.WHITE?PlayerColor.BLACK:PlayerColor.WHITE);
        });
        constraints.gridx = 2;
        constraints.gridy = maxJ/2;
        MainPanel.add(close,constraints);
        new TextToTranslationJButton(close, "surrender");

        //turno
        constraints.gridx = 0;
        constraints.gridy = 0;
        MainPanel.add(turno,constraints);

        //tempo
        constraints.gridx = 1;
        constraints.gridy = maxJ/2;
        MainPanel.add(time,constraints);

        //eatList
        constraints.gridx = 2;
        constraints.gridy = 0;
        MainPanel.add(eatList,constraints);
        
        
        //FRAME OPTIONS
        this.setResizable(false);
        add(MainPanel);
        setSize(800, 600);
        pack();
        setVisible(true);
        
        this.addWindowListener(new java.awt.event.WindowAdapter() {
            @Override
            public void windowClosing(java.awt.event.WindowEvent windowEvent) {
                close();
            }
        });
        
        //UpdateGUI();
    }

    /**
     * Update the on screen remaining time to do a move.
     * @param rtime Remaining time to display.
     */
    public void updateRemainingTime(int rtime)
    {
        time.setText("<html><center><font color='white'>"+Translations.getTranslation("remainingtime")+"<br>"+rtime+"</font></center>");
    }

    /**
     * Update the Board GUI according to current configuration.
     */
    public void UpdateGUI()
    {
    	String currentColor=matchManager.getGameLogic().getCurrentPlayerColor()==PlayerColor.BLACK?"<font color='black'>"+Translations.getTranslation("black")+"</font>":"<font color='white'>"+Translations.getTranslation("white")+"</font>";
    	turno.setText("<html><font color='yellow'>"+Translations.getTranslation("turn")+"</font><font color='red'><br>"+matchManager.players[matchManager.getGameLogic().getCurrentPlayer()].nickname+"<br>"+currentColor);
		Cell cells[][]=matchManager.getBoard().getCellMatrix();
        for (int i=0; i< maxI; i++)
        {
        	for (int j=0; j<maxJ; j++)//crea in base a com� la scacchiera e a quali pezzi ci sono
        	{
        		Optional<Piece> cell=cells[i][j].getPiece();
        		if (cell.isPresent())
        		{
        			Piece piece=(Piece) cell.get();
        			String prefix=piece.getOwner()==PlayerColor.BLACK?"b_":"w_";
                    setSprite(celle[i][j],prefix+piece.getIcon());
        		}
        		else
                    setSprite(celle[i][j],null);
        	}
        }
        updateEatList();
    }
    

    /**
     * Close the current match.
     */
    private void close()
    {
    	matchManager.getGameLogic().gameEnded=true;
    	setVisible(false);
    	Thread.currentThread().interrupt();
    }
    

    /**
     * Set all cells color to standard color
     */
    private void resetAllCellsColors()
    {
    	for (int i=0; i<maxI; i++)
    	{
        	for (int j=0; j<maxJ; j++)
        	{
        		setCellColor(i,j);
        	}
    	}
    }
    
    /**
     * Set a cell to its standard color.
     * @param x X of the cell.
     * @param y Y of the cell.
     */
    private void setCellColor(int x, int y)
    {
    	if (((x*8+y)+(x%2))%2==0)
    	{
			celle[x][y].setBackground(Color.WHITE);
			celle[x][y].setForeground(Color.BLACK);
    	}
    	else
    	{
			celle[x][y].setBackground(Color.BLACK);
			celle[x][y].setForeground(Color.WHITE);
    	}
    }

    /**
     * Find a pressed button's position.
     * @param c Pressed button.
     */
    private BoardPosition findButton(Object c) {
        for (int x = 0; x < celle.length; x++) {
            for (int y = 0; y < celle[0].length; y++) {
                if (c.equals(celle[x][y])) {
                    return new BoardPosition(x,y);
                }
            }
        }
		return null;
    }

    /**
     * Open an Help GUI.
     */
    private void openHelp()
    {
    	new HelpGUI();
    }

    /**
     * Called whenever a cell is pressed.
     * @param bPosition BoardPosition of the pressed cell.
     */
    private void onCellPressed(BoardPosition bPosition)
    {
    	if (!matchManager.getGameLogic().isPlayerTurn())
    	{
    		System.out.println("E' il turno della IA!");
    		return;
    	}
    	
    	int x=bPosition.getX();
    	int y=bPosition.getY();
    	if (lastSelectedCell==null)
    	{
    		//se ho selezionato un pezzo ed � mio
    		if (matchManager.getBoard().getCellMatrix()[x][y].getPiece().isPresent() && matchManager.getBoard().getCellMatrix()[x][y].getPiece().get().getOwner()==matchManager.getGameLogic().getCurrentPlayerColor())
    		{
        		lastSelectedCell=new BoardPosition(x,y);
        		celle[x][y].setBackground(Color.RED);
        		celle[x][y].setForeground(Color.WHITE);
            	//gamelogic.Move(origin,destination);
        		List<BoardPosition> moves = matchManager.getBoard().getCellMatrix()[x][y].getPiece().get().getPossibleMoves(matchManager.getGameLogic().getChessBoard());
        		if (moves!=null)
        		{
        			if (settings.GlobalSettingsImpl.isHintEnabled)
        			{
                		for (int i=0; i<moves.size(); i++)
                		{
                			int xpos=moves.get(i).getX();
                			int ypos=moves.get(i).getY();
                			if (xpos<0 || ypos<0 || xpos>=maxI || ypos>=maxJ)
                				continue;
                    		celle[xpos][ypos].setBackground(Color.YELLOW);
                    		celle[xpos][ypos].setForeground(Color.WHITE);
                		}
        			}
        		}
        		else
        		{
        			System.out.println("ERRORE: getPossibleMoves ritorna NULL?!");
        		}
    		}
    	}
    	else
    	{    		

    		//setCellColor(lastSelectedCell.getX(),lastSelectedCell.getY());
    		resetAllCellsColors();
    		
    		if (lastSelectedCell.getX()==x && lastSelectedCell.getY()==y)//annulla mossa
    		{
        		lastSelectedCell=null;
    			return;
    		}

    		boolean isPossibleMove=false;
    		List<BoardPosition> moves = matchManager.getBoard().getCellMatrix()[lastSelectedCell.getX()][lastSelectedCell.getY()].getPiece().get().getPossibleMoves(matchManager.getGameLogic().getChessBoard());
    		if (moves!=null)
    		{
        		for (int i=0; i<moves.size(); i++)
        		{
        			if (moves.get(i).getX()== x && moves.get(i).getY()==y)
        			{
        				isPossibleMove=true;
        				break;
        			}
        				
        		}
        		if (!isPossibleMove)
        		{
   				 	new WarningGUI(Translations.getTranslation("cantmovethere"));
            		lastSelectedCell=null;
        			return;
        		}
    		}

			matchManager.getGameLogic().DoMove(lastSelectedCell, new BoardPosition(x,y),true);
			
        	lastSelectedCell=null;
    		UpdateGUI();
    	}
    }

    /**
     * Set a sprite on a cell.
     * @param assign a piece sprite to a chessboard cell
     * @param spriteName The name of the sprite (Loaded from the current skin pack)
     */
    private void setSprite(JButton button, String spriteName)
    {
    	button.setIcon(getSprite(spriteName));
    }

    /**
     * Get a piece sprite from it's name.
     * @param spritename Name of the sprite to load from the current skin pack.
     * @return Selected sprite.
     */
    private ImageIcon getSprite(String spritename)
    {
    	if (spritename==null) return null;
        ImageIcon image=new ImageIcon();
        try {
			BufferedImage bufferedImage = ImageIO.read(new File(getCurrentSpritePackPath(spritename)));
			image =new ImageIcon(bufferedImage.getScaledInstance( squareSize, squareSize,  java.awt.Image.SCALE_FAST ));

		} catch (IOException e) {
			System.out.println(e.getMessage());
		}
        return image;
    }

    /**
     * Get the path to a sprite on the current selected sprite pack.
     * @param spriteName The sprite to search.
     * @return The path of the sprite.
     */
    private String getCurrentSpritePackPath(String spriteName)
    {
    	if (spriteName!=null)
    	{
        	String path="/resources/"+matchManager.resourcesPath+"/";
        	return CrossPlatformResources.getResourceFile(path+spriteName).getPath();
    	}
		return null;
    }

    /**
     * Open a End game GUI.
     * @param winner name of the winner.
     * @param score score of the winner.
     * @param wasLastMatch If false player will be able to open another match after this.
     */
    public void printEndedMatch(String winner, int score, boolean wasLastMatch)
    {
		//System.out.println("ha vinto "+winner);
    	new EndGameGUI(winner, score, !wasLastMatch,this).setVisible(true);;
    }
    /**
     * What to so when game ends.
     */
    public void OnEndGameGUIClosed()
    { 
    	matchManager.NextMatch();
    	close();
    }

    /**
     * Update on screen eat list.
     */
    public void updateEatList(){
    	String eatListText=matchManager.getGameLogic().eat.size()>0? "<html><font color='yellow'>"+Translations.getTranslation("eatlist")+"</font>":"";
    	for (Piece p: matchManager.getGameLogic().eat)
    	{
    		String colorStart=p.getOwner()==PlayerColor.WHITE?"<font color='white'>":"<font color='black'>";
    		eatListText+="<br>>"+colorStart+Translations.getTranslation(p.getIcon())+"</font>";
    	}
    	eatList.setText(eatListText);
    }
}