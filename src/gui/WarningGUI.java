package gui;

import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import Sound.Audio;

/**
 * 
 * Riccardo Foschi
 * 
 * Classe utilizzata per generare messaggi all'utente,principalmente errori
 *
 */
public class WarningGUI extends JFrame{
	
	

	private static final long serialVersionUID = 1L;//altirmenti da warnings a caso
	
	JButton close= new JButton("close");
	JLabel label;
	JPanel panel= new JPanel();
	
	ActionListener ac = (e)->{
		Audio.playSound("/clips/button_01.wav");//fa suono
		this.setVisible(false);
	};
	public WarningGUI(String text) {

		super("Warning");
		this.label=new JLabel(text);
		panel.add(label);
		panel.add(close);
		close.addActionListener(ac);
		
        add(panel);
        setSize(800, 600);
        pack();
        setVisible(true);		
	}
}
