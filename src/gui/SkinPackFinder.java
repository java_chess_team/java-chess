package gui;

/**
 * 
 * @author Marco Amadei
 * 
 * Gestione e trovamento degli skin pack installati
 *
 */
import java.io.File;
import java.io.FileFilter;

import settings.CrossPlatformResources;

public class SkinPackFinder {

	
	//Lista di skin folders gia conosciute (per i .JAR)
	private static String[] knownJARSkinFolders=new String[] {"standard","golden"};

	
	
    /**
     * Get paths to all installed folders that contains a skin pack.
     * @return Path of the installed skin packs.
     */
	public static String[] getSkinFolders()	{
		if (CrossPlatformResources.isJAR())	{
			return knownJARSkinFolders;
		}
		else{
	    	String resources_path="/resources/";
	       
			File[] directories = CrossPlatformResources.getResourceFile(resources_path).listFiles(new FileFilter() {
			    public boolean accept(File file) {
			        return file.isDirectory();
			    }
			});
			

			return DirectoriesToStrings(directories);
		}
	}
	
	
    /**
     * Translate an array of Files into an array of string of the Paths to those files.
     * @param files Files to translate into string paths.
     * @return A string array of all paths.
     */
	public static String[] DirectoriesToStrings(File[] files)
	{
		String[] ret=new String[files.length];
		for (int i=0; i<files.length; i++)
			ret[i]=files[i].getName();
		return ret;
	}
}
