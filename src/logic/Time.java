package logic;

import java.net.MalformedURLException;
import ai.exceptions.StockfishInitException;
import settings.MatchSettings.GameTime;
/**
 * 
 * Riccardo Foschi
 * 
 * Thread che simula un timer
 *
 */
public class Time implements Runnable {
	
	private int maxTime;
	private int currentTime=0;
	Thread thread;
	LogicGame logic;
	private boolean ignoreChangeRounder=false;
	
	public Time(GameTime time,LogicGame logic) {
		
		switch (time) {
		case disabled:
			maxTime=0;
			break;

		case max10Seconds:
			maxTime=10;
			break;
			
		case max30Sedonds:
			maxTime=30;
			break;
			
		case max2Minutes:
			maxTime=120;
			break;
			
		case max5Minutes :
			maxTime=300;
			break;
			
		case max10Minutes:
			maxTime=600;
			break;	
		
		}
		thread= Thread.currentThread();
		this.logic=logic;
	}
	

	/**
	 * Imposta se deve cambiare il round
	 * 
	 */
	
	public void setIgnoreTimer() {
		this.ignoreChangeRounder=true;
	}
	
	/**
	 * Fa ripartire il timer 
	 */
	public void restart() {
		currentTime=0;
		thread.start();
	}
	/**
	 * Ritorna true se è in funzione il thread
	 * 
	 */
	public boolean isRunning() {
		return thread.isAlive();				
	}
	
	/**
	 * Ritorna il tempo massimo preImpostato
	 * 
	 */
	public int getMaxTimer() {
		return this.maxTime;
	}
	
	/**
	 *  Ritorna il secondo corrente
	 * 
	 */	
	public int getCurrentTime() {
		return currentTime;
	}
	
	/**
	 * Ritorna il tempo in maniera inversa 
	 * 
	 */
	public int getTimeInversed() {
		return maxTime-getCurrentTime();
	}
	
	/**
	 * Ritorna se è attivo
	 * 
	 */
	public boolean isActive()
	{
		return maxTime>0;
	}
	
	/**
	 *  Play threads  
	 */
	@Override
	public void run() {
		if (isActive())
		{
			try {
				
				for(int i=0;i<maxTime;i++) {					
					currentTime++;//è passato un secondo
					Thread.sleep(1000);//mi sleepa per un secodno
					if(this.ignoreChangeRounder)
						break;
					else
						logic.OnRemainingTimeUpdated(getTimeInversed());
				}
				if(!this.ignoreChangeRounder){
					logic.changeRounders();//cambio forzatamente 
					logic.startTurn();
				}
				//restart();//faccio partire
			} catch (InterruptedException | MalformedURLException | StockfishInitException e) {
				
				e.printStackTrace();
			}
		}					
	}
	
}
